#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>

#define N 32
#define M 256
#define MAX 50
enum{
    L=0x1,  //登录
    C,  //聊天
};

typedef struct{
    int type;//消息类型
    char name[N];
    char text[M];//消息正文
}MSG;


#define LEN sizeof(MSG)

/*
    生成用户链表
*/

/*
    1.把新注册用户登陆消息告诉其它用户
    2.把新用户插入到用户链表中
    3.服务器打印一下
*/
void do_login(int sockfd,struct sockaddr_in clientaddr,MSG msg,int *last,struct sockaddr_in *msg_arry)
{
	int i = 0;
	char buf[M] = "";

	sprintf(buf,"%s %s",msg.name,"login");
	if(*last == -1)
	{
		puts(buf);
		msg_arry[++*last] = clientaddr;
		printf("%d\n",*last);
		return;
	}
	while(i <= *last)
	{
		sendto(sockfd, buf, M, 0,(struct sockaddr *)&msg_arry[i], sizeof(struct sockaddr ));
		i++;
	}
	msg_arry[++*last] = clientaddr;
	printf("%d\n",*last);
	puts(buf);
}

/*
    1.把聊天信息发给其它用户
    2.发给自己一份
*/
void do_chat(int sockfd,struct sockaddr_in clientaddr,MSG msg,int *last,struct sockaddr_in *msg_arry)
{
	int i = 0;
  	if(*last == -1)
	{
		return;
	}
	char buf[M] = "";
	sprintf(buf,"%s %s %s",msg.name,"said:",msg.text);
	while(i <= *last)
	{
		if(!memcmp(&clientaddr,&msg_arry[i],sizeof(struct sockaddr_in)))
		{
			i++;
			continue;
		}
		sendto(sockfd, buf, M, 0,(struct sockaddr *)&msg_arry[i], sizeof(struct sockaddr ));
		i++;
	}
	puts(buf);
}

int main(int argc, const char *argv[])
{
    int sockfd;
    struct sockaddr_in serveraddr, clientaddr;
    MSG msg;
    pid_t pid;
	struct sockaddr_in msg_arry[MAX];
	int last;
    socklen_t len=sizeof(struct sockaddr);
    
    if(argc!=3){
        printf("user:%s ip port",argv[0]);
        return -1;
    }

    //创建socket
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0){
        perror("socket err");
        exit(-1);
    }

    //绑定服务器地址
    bzero(&serveraddr,len);
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    if(bind(sockfd, (struct sockaddr *)&serveraddr, len) < 0){
        perror("bind err");
        exit(-1);
    }
	
    //完成消息转发
	pid = fork();
	if(pid < 0)
	{
		printf("fail to fork\n");
		return -1;
	}
	else if(pid > 0)
	{
		last = -1;
		while(1)
		{
			recvfrom(sockfd, &msg, LEN, 0,(struct sockaddr *)&clientaddr, &len);
			switch (msg.type)
			{
				case C:
					do_chat(sockfd,clientaddr,msg,&last,msg_arry);
					break;
				case L:
					do_login(sockfd,clientaddr,msg,&last,msg_arry);
					break;

				default:
					printf("err msg\n");
			}
			memset(&msg,0,M);
		}
	}
	else
	{
	//	while(1)
		{
			scanf("%s",msg.text);
			getchar();
			msg.type = C;
			strcpy(msg.name,"server");
			sendto(sockfd, &msg, LEN, 0,(struct sockaddr *)&serveraddr, len);
		}
	}
	return 0;
}
