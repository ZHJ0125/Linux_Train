#include <stdio.h>
#include <sys/types.h>			/* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>			/* superset of previous */
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PORT 8888
#define N 64

int main()
{
	int client_fd;
	int ret;
	char buf[N] = "";
	struct sockaddr_in server_addr;
	socklen_t addrlen = sizeof(server_addr);
	client_fd = socket(AF_INET,SOCK_STREAM,0);

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	ret = connect(client_fd, (struct sockaddr *)&server_addr, addrlen);
	if(ret == -1)
	{
		perror("Fail to Connect");
		return -1;
	}
	printf("Success to connect\n");
	printf("Server ip: %s\n",inet_ntoa(server_addr.sin_addr));

	while(1)
	{
		scanf("%s",buf);
		getchar();
		send(client_fd, buf, strlen(buf), 0);
		printf("Send: %s\n",buf);
		memset(buf,0,N);
	}

	return 0;
}
