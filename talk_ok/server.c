#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>

#define N 32
#define M 256

enum{
    L=0x1,  //登录
    C,  //聊天
};

typedef struct{
    int type;//消息类型
    char name[N];
    char text[M];//消息正文
}MSG;

typedef struct node{   //用户结点
	struct sockaddr_in addr;  //地址
	struct node *next; //指向下一个用户结点
}linknode,*linklist;

#define LEN sizeof(MSG)

/*
    生成用户链表
*/
linklist client_list_create()
{
    linklist head = (linklist)malloc(sizeof(linknode));
    if(NULL == head){
        return NULL;
    }else{
        head->next = NULL;
        return head;
    }
}

/*
    1.把新注册用户登陆消息告诉其它用户
    2.把新用户插入到用户链表中
    3.服务器打印一下
*/
void do_login(int sockfd,struct sockaddr_in clientaddr,linklist head,MSG msg)
{
    //发送
    linklist p, q;

    p = head->next;
    sprintf(msg.text,"%s login.....",msg.name);
    while(p){
        sendto(sockfd, &msg, LEN, 0, (struct sockaddr *)&p->addr, sizeof(struct sockaddr));
        p = p->next;
    }

    //头插
    q = (linklist)malloc(sizeof(linknode));
    q->addr = clientaddr;

    q->next = head->next;
    head->next = q;

    puts(msg.text);
}

/*
    1.把聊天信息发给其它用户
    2.发给自己一份
*/
void do_chat(int sockfd,struct sockaddr_in clientaddr,linklist head,MSG msg)
{
    linklist p;

    char buf[M] = {0};
    p = head->next;
    
	//sprintf(msg.text, "%s said %s", msg.name, msg.text); //不允许这样用
    sprintf(buf, "%s said %s", msg.name, msg.text);
    strcpy(msg.text, buf);
    
    while(p){
        if(memcmp(&p->addr, &clientaddr, sizeof(clientaddr)) != 0){  //不给自己发送
            sendto(sockfd, &msg, LEN, 0, (struct sockaddr *)&p->addr, sizeof(struct sockaddr));
        }
        p = p->next;
    }
  
    puts(msg.text);
}

int main(int argc, const char *argv[])
{
    int sockfd;
    struct sockaddr_in serveraddr, clientaddr;
    MSG msg;
    pid_t pid;
    socklen_t len=sizeof(struct sockaddr);
    linklist head;
    
    if(argc!=3){
        printf("user:%s ip port",argv[0]);
        return -1;
    }

    //创建socket
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0){
        perror("socket err");
        exit(-1);
    }

    //绑定服务器地址
    bzero(&serveraddr,len);
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    if(bind(sockfd, (struct sockaddr *)&serveraddr, len) < 0){
        perror("bind err");
        exit(-1);
    }

    //fork--子进程发送消息，父进程接收消息并处理
    pid = fork();
    if(pid < 0){
        perror("fork err");
        exit(-1);
    }else if(pid == 0){  //子进程
        while(1){
            strcpy(msg.name, "server");
            gets(msg.text);
            msg.type = C;
            sendto(sockfd, &msg, LEN, 0, (struct sockaddr *)&serveraddr, len);
        }
    }else{
        //创建客户端链表
        head = client_list_create();
        if(NULL == head){
            printf("link create err\n");
            exit(-1);
        }
        while(1){
            recvfrom(sockfd, &msg, LEN, 0, (struct sockaddr *)&clientaddr, &len);

            switch(msg.type){
                case L:  //如果收到的是登陆信息
                    do_login(sockfd, clientaddr, head, msg);
                    break;
                    
                case C:  //收到了聊天信息
                    do_chat(sockfd, clientaddr, head, msg);
                        break;

                default:
                    puts("err msg type");
            }
        }
    }

}
