#include <stdio.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define N 64

int main(int argc, const char *argv[])
{
	int client_fd;
	int ret;
	char buf[N] =  "";
	struct sockaddr_in server_addr,client_addr;
	socklen_t addrlen = sizeof(struct sockaddr_in);
	pid_t pid;

	client_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(client_fd == -1)
	{
		perror("Fail to socket");
		return -1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(atoi(argv[2]));
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);

//	recvfrom(client_fd, buf, N, 0,(struct sockaddr *)&server_addr, &addrlen);
	pid = fork();
	if(pid > 0)
	{
		while(1)
		{
			recvfrom(client_fd, buf, N, 0,(struct sockaddr *)&server_addr, &addrlen);
			printf("Recive : %s\n",buf);
			memset(buf,0,N);
		}
	}
	else if(pid == 0)
	{
		while(1)
		{
			scanf("%s",buf);
			getchar();
			sendto(client_fd, buf, sizeof(buf), 0,(struct sockaddr *)&server_addr, addrlen);
			memset(buf,0,N);
		}
	}

	return 0;
}
