#include <stdio.h>
#include <netinet/in.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc,const char *argv[])
{
	int server_fd;
	struct sockaddr_in server_addr,client_addr;
	int ret;
	char buf[64] = "";
	pid_t pid;
	socklen_t len = sizeof(struct sockaddr_in);
	server_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(server_fd == -1)
	{
		perror("fail to socket\n");
		return -1;
	}
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(atoi(argv[2]));
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);
	
	ret = bind(server_fd, (struct sockaddr *)&server_addr,len);
	if(ret == -1)
	{
		perror("fail to bind\n");
		return -1;
	}
	
	recvfrom(server_fd, buf, 64, 0,(struct sockaddr *)&client_addr, &len);
	printf("%s\n",buf);
	pid = fork();
	if(pid > 0)
	{
		while(1)
		{
			recvfrom(server_fd, buf, 64, 0,(struct sockaddr *)&client_addr, &len);
			printf("%s\n",buf);
			memset(buf,0,64);
		}
	}
	else if(pid == 0)
	{
		while(1)
		{
			scanf("%s",buf);
			getchar();
			sendto(server_fd, buf, strlen(buf), 0,(struct sockaddr *)&client_addr, len);
		}
	}
}
