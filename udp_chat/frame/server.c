#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>

#define N 32
#define M 256

enum{
    L=0x1,  //登录
    C,  //聊天
};

typedef struct{
    int type;//消息类型
    char name[N];
    char text[M];//消息正文
}MSG;


#define LEN sizeof(MSG)

/*
    生成用户链表
*/

/*
    1.把新注册用户登陆消息告诉其它用户
    2.把新用户插入到用户链表中
    3.服务器打印一下
*/
void do_login(int sockfd,struct sockaddr_in clientaddr,MSG msg)
{
  
}

/*
    1.把聊天信息发给其它用户
    2.发给自己一份
*/
void do_chat(int sockfd,struct sockaddr_in clientaddr,MSG msg)
{
  
}

int main(int argc, const char *argv[])
{
    int sockfd;
    struct sockaddr_in serveraddr, clientaddr;
    MSG msg;
    pid_t pid;
    socklen_t len=sizeof(struct sockaddr);
    linklist head;
    
    if(argc!=3){
        printf("user:%s ip port",argv[0]);
        return -1;
    }

    //创建socket
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0){
        perror("socket err");
        exit(-1);
    }

    //绑定服务器地址
    bzero(&serveraddr,len);
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    if(bind(sockfd, (struct sockaddr *)&serveraddr, len) < 0){
        perror("bind err");
        exit(-1);
    }

    //完成消息转发

}
