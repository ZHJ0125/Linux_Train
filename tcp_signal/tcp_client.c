#include <stdio.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#define N 64

int client_fd;
void fun_send(int num)
{
		char buf[N] = "";
		scanf("%s",buf);
		getchar();
		send(client_fd, buf, strlen(buf), 0);
		memset(buf,0,64);
}
int main(int argc,const char *argv[])
{
	char buf[N] = "";
	int ret;
	struct sockaddr_in server_addr;

	client_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(client_fd == -1)
	{
		perror("fail to socket");
		return -1;
	}
	memset(&server_addr,0,sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(atoi(argv[2]));
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);

	ret = connect(client_fd, (struct sockaddr *)&server_addr,sizeof(server_addr));
	if(ret == -1)
	{
		perror("fail to connect\n");
		return -1;
	}
	printf("Connect success\n");
//	signal(SIGINT,fun_send);
	while(1)
	{
		scanf("%s",buf);
		ret = send(client_fd, buf, strlen(buf), 0);
		if(ret == -1)
		{
			perror("fail to send");
			return -1;
		}
		else if(ret > 0)
		{
			printf("Send : %s\n",buf);
			memset(buf,0,N);
		}
	}
	return 0;
}
